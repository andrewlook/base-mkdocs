DIRNAME := $(shell pwd)


##########################################################
#
# Docs
#
##########################################################

serve-mkdocs:
	mkdocs serve

build-mkdocs:
	mkdocs build

# For now, run this manually when you add a notebook. We can have CI do this
# automatically if it's too much friction to do this after adding a notebook
# to the docs folder.
nbconvert-docs: clean-nbconvert-docs
	for f in `find -d ./docs -iname "*.ipynb"`; do echo "converting $$f"; jupyter nbconvert --to html $$f; done;

clean-nbconvert-docs:
	find -d ./docs -iname ".ipynb_checkpoints"
	find -d ./docs -iname ".ipynb_checkpoints" | xargs rm -rf
	find -d ./docs -iname "*.html"
	find -d ./docs -iname "*.html" | xargs rm -rf
